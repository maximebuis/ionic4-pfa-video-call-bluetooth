import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

import { catchError, tap } from 'rxjs/operators';
import {  throwError, Observable } from 'rxjs';
import { retry,  } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Storage } from '@ionic/storage';
@Injectable()
export class RedditService {

URLbase = "https://api.interphone-pfa.fr/";
language: string="EN";
exp: any;
token: any;
headers: HttpHeaders;
  email: any;


constructor(public http: HttpClient, public storage: Storage ) {

  this.storage.get('token').then((val) => {
    this.token=val;
  console.log(this.token);
  });

  this.storage.get('token').then((token) => {
    console.log('TOKEN ');
    this.token="token";
    console.log(this.token);
    // Http Options
  this.httpOptions = {
    headers: new HttpHeaders({
      
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' +'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzEiLCJlbWFpbCI6ImRlbW9AZ21haWwuY29tIn0.CGVzsWyK3f1LOnsDQKUlQBOa2mp5AO1_s_s9ZnLstps',
    }) 
  }
  });
}



ngOnInit() {



this.storage.get('token').then((val) => {
  this.token=val;
console.log(this.token);
});
  this.storage.get('token').then((token) => {
    console.log('TOKEN ');
    this.token=token;
    console.log(this.token);
    // Http Options
  this.httpOptions = {
    headers: new HttpHeaders({
      
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' +'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzEiLCJlbWFpbCI6ImRlbW9AZ21haWwuY29tIn0.CGVzsWyK3f1LOnsDQKUlQBOa2mp5AO1_s_s9ZnLstps',
    }) 
  }
  });

}

/*
ionViewWillEnter() {
  this.httpOptions = {
    headers: new HttpHeaders({
      
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' +'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEyMzEiLCJlbWFpbCI6ImRlbW9AZ21haWwuY29tIn0.CGVzsWyK3f1LOnsDQKUlQBOa2mp5AO1_s_s9ZnLstps',
    }) 
  }
}*/

// Http Options
httpOptions = {}


/////////////APP


/////// Lis
getPageList(): Observable<any> {
  return this.http
    .get<any>(this.URLbase + 'getPageList',this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

/////Login
login(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'login',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

getDataBypage(page,table,category,status): Observable<any> {
  return this.http
    .get<any>(this.URLbase + 'dataByPage/'+page+'?table='+table+'&category='+category +'&status='+status, this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

////USER
updateUser(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updateuser',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

updatepassword(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updatepassword',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

updateuserpassword(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updateuserpassword',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

///// REST API
postByid(table,id): Observable<any> {
  return this.http
    .get<any>(this.URLbase + 'post/'+table+'?id='+id,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}
addPost(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'addPost',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

updatePostTable(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updatePostTable',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

deleteTable(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'deleteTable',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

searchByid(page,table,category,status,wordid): Observable<any> {
    return this.http
    .get<any>(this.URLbase + 'searchByid/'+page+'?table='+table+'&category='+category +'&status='+status +'&wordid='+wordid,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

searchByword(page,table,category,status,word,word1,word2): Observable<any> {
  return this.http
  .get<any>(this.URLbase + 'searchByword/'+page+'?table='+table+'&category='+category +'&status='+status +'&word='+word+'&word1='+word1+'&word2='+word2,this.httpOptions)
  .pipe(retry(2),catchError(this.handleError))
}

  
///////////////PAGES////////////////////// 

addPage(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'addPage',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

updatePage(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updatePage',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

//////////////////USERS//////////////////

userExist(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'userexist',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

register(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'register',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

updateUserAdmin(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updateuseradmin',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

////////////FONCTION PFA
usersbyfkid(page,fkuser): Observable<any> {
  return this.http
  .get<any>(this.URLbase +'usersbyfkid/'+page+'?fkuser='+fkuser,this.httpOptions)
  .pipe(retry(2),catchError(this.handleError))
}


/////////////////////////WEBRTC////////////////////////////////
//update password
UpdateWebrtc(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updatewebrtc',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}

///////////////////////////LISTING/////////////////////////////
notif(page,fkuser): Observable<any> {
  return this.http
  .get<any>(this.URLbase +'notifsbyfkid/'+page+'?fkuser='+fkuser,this.httpOptions)
  .pipe(retry(2),catchError(this.handleError))
}

listingbyiduser(fkuser): Observable<any> {
  return this.http
  .get<any>(this.URLbase +'listingbyiduser/'+fkuser,this.httpOptions)
  .pipe(retry(2),catchError(this.handleError))
}


notifbyid(idinterphone): Observable<any> {
  return this.http
  .get<any>(this.URLbase +'notif/'+idinterphone,this.httpOptions)
  .pipe(retry(2),catchError(this.handleError))
}


updateNotif(data): Observable<any> {
  return this.http
    .post<any>(this.URLbase +'updatenotif',data,this.httpOptions)
    .pipe(retry(2),catchError(this.handleError))
}


////////////////////LOG////////////////////////////////////// 
/** Log a HeroService message with the MessageService */
private log(message: string) {
  console.log(message);
}
// Handle API errors
handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    console.error('An error occurred:', error.error.message);
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    console.error(
      `Backend returned code ${error.status}, ` +
      `body was: ${error.error}`);
  }
  // return an observable with a user-facing error message
  return throwError(
    'Something bad happened; please try again later.');
};

}
