import { Component } from '@angular/core';
import { NavController, PopoverController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { RedditService } from 'src/providers/reddit-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  iduser: any;
  id: number;
  table: string="customers_auth";
  category:string="page";
  view:boolean;
  push: boolean=false;
  data: any;
  posts: any;
  content: string="";
  image:string="";
  title: string="";
  viewdata: any;
  lastname: any;
  firstname: any;
  email: any;
  address: any;
  city: any;
  cp: any;
  phone: any;
  label: any;
  token: any;

  
    constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,public alertController: AlertController,private route: ActivatedRoute,
      public loadingController:LoadingController, public redditService:RedditService, private router: Router,  public toastCtrl: ToastController,
      private storage: Storage, private alertCtrl: AlertController) {

        this.storage.get('iduser').then((val) => {
          this.token=val;
        console.log(this.token);
        });
  
     }

     ionViewWillEnter(){
      this.storage.get('iduser').then((val) => {
        this.id=val;
        this.redditService.postByid(this.table, this.id).subscribe(data=>{
          this.posts=data.items; 
          this.image=data.items[0].image;
        })
      });
     }


  ngOnInit() {
    this.storage.get('iduser').then((val) => {
      this.id=val;
      this.redditService.postByid(this.table, this.id).subscribe(data=>{
        this.posts=data.items; 
        this.image=data.items[0].image;
      })
    });
  }

  doRefresh(event) {
    setTimeout(() => {
      this.storage.get('iduser').then((val) => {
        this.id=val;
        this.redditService.postByid(this.table, this.id).subscribe(data=>{
          this.posts=data.items; 
          this.image=data.items[0].image;
        })
      });
      event.target.complete();
    }, 2000);
  }

  async edit() {
    this.router.navigateByUrl('/edit-profile');
   }

}