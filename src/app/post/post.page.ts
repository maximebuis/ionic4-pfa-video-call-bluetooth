import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Component, OnInit } from '@angular/core';
import { NavController, LoadingController, PopoverController, AlertController, ToastController } from '@ionic/angular';
import { RedditService } from 'src/providers/reddit-service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {

  id: number;
  table: string="posts";
  category:string="article";
  view:boolean;
  push: boolean=false;
  data: any;
  posts: any;
  content: string="";
  image:string="";
  title: string="";
  viewdata: any;


  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,public alertController: AlertController,private route: ActivatedRoute,
    public loadingController:LoadingController,  public redditService:RedditService, private router: Router,  public toastCtrl: ToastController) {

   }

  ngOnInit() {
   this.route.params.subscribe(params => {
      this.id = params['id']; 
  });
  
  this.redditService.postByid(this.table, this.id).subscribe(data=>{
    this.title=data.items[0].title;   
    this.content=data.items[0].content;    
    this.image=data.items[0].image;   
  })
  }




    
}
