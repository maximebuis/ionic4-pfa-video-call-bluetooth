import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { RedditService } from 'src/providers/reddit-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.page.html',
  styleUrls: ['./forgotpassword.page.scss'],
})

export class ForgotpasswordPage implements OnInit {

  public onRegisterForm: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;
  formgroup: FormGroup;
  new: string="";
  password2: string | Int32Array;
  email: string = "";
  public parameter1: string;
  id: any;
  hide=0;
  constructor( public navCtrl: NavController, 
    public redditService: RedditService,public storage: Storage,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    private router: Router) {
    this.email;
  }

  ngOnInit() {
    this.onRegisterForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])]
    });

    this.storage.get('iduser').then((val) => {
      this.id=val;
  });
  }

  validation_messages = {
   
    'email': [
      { type: 'required', message: 'Email est requis.' },
      { type: 'pattern', message: 'Entrez un email valide' }
    ]
  };
  async do() {
    var data = JSON.stringify({
      email: this.email,
    });
    this.redditService.userExist(data)
      .toPromise()
      .then(async (response) => {

        if (response[0].nbrow >= 1) {
            this.sendnewwpassword();
        } else {


          const alert = await this.alertController.create({
            header: 'Désolé',
            message: 'Utilisateur inconnu ',
            buttons: [
              {
                text: 'Annuler',
                role: 'cancel',
                cssClass: 'secondary',
                handler: (blah) => {
                }
              }, {
                text: 'Oui',
                handler: () => { 
                  setTimeout(() => { 
                  
                 }, 500); 
                }}]
            });
          await alert.present();

        }
      })
      
  }

  async sendnewwpassword(){


    var data = JSON.stringify({
      email: this.email,
    });
    if(this.hide<1){
      this.hide=this.hide+1
    
    this.redditService.updatepassword(data).toPromise()
    .then(async (response) => {
      if(response.status == 'success') {

        const alert = await this.alertController.create({
          header: 'Nouveau mot de passe',
          message: 'Consultez votre boite e-mail.',
          buttons: ['OK']
        });
    
        await alert.present();
 
      this.storage.set('password',"");
      setTimeout(() => {

        this.router.navigateByUrl('/login');
       }, 300);



    } else {

      const alert = await this.alertController.create({
        header: 'Erreur',
        message: 'Problème de connexion au serveur.',
        buttons: ['OK']
      });
  
      await alert.present();
    }
})    
  }}

  

}
