import { Component } from '@angular/core';
import {Storage} from '@ionic/storage';
import { RedditService } from 'src/providers/reddit-service';
import { NavController, PopoverController, LoadingController, ToastController, AlertController, Platform } from '@ionic/angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { Router } from '@angular/router';
import { AuthService } from 'src/providers/authService';
import { NativeRingtones } from '@ionic-native/native-ringtones/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
declare var apiRTC: any;


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  pages: any;
  items: any;
  posts: any;
  title:any;
  page:number;
  region: string;
  currentpage: number;
  table: string="posts";
  word1: string="title";
  word2: string="content";
  category:number=0;
  word: any="";
  wordid: any="";
  fkuser: number;
  item: any;

  showCall: boolean;
  showHangup: boolean;
  showAnswer: boolean;
  showReject: boolean;
  showStatus: boolean;
  showRemoteVideo: boolean = false;
  showMyVideo: boolean = false;
  showCommand: boolean=false;
  showHistory: boolean=true;

  session;
  webRTCClient;
  incomingCallId = 0;
  myCallId;
  calleeId;
  iduser: any;
  iduserapi: any;
  webrtc: any;
  status: any;
  id: any;
  nameblue: any;
  idinterphone: any;
  idnotif: any;
  audio1: any;
  sound: boolean;
  constructor(public navCtrl: NavController, public storage: Storage,
    public popoverCtrl: PopoverController, public redditService: RedditService,public alertController: AlertController,private router: Router, 
    public toastCtrl: ToastController, public loadingController:LoadingController,  public loadingCtrl: LoadingController,public nativeAudio: NativeAudio,
    public authService: AuthService,   private platform: Platform, private ringtones: NativeRingtones,    private androidPermissions: AndroidPermissions) {
   
   
    this.checkPermissions();
    this.InitializeApiRTC();
    this.ringtones.getRingtone().then((ringtones) => { console.log(ringtones); });
  }


  ionViewWillEnter(){


    this.getnotif();
    this.infointerphone();
    this.reset();
  }

  ngOnInit() {
    this.page=1;
    console.log( this.fkuser);
 
    this.storage.get('customer_id').then((iduser) => {
      this.iduserapi=iduser;    
    })

    this.storage.get('iduser').then((iduser) => {
      this.iduserapi=iduser;    
    })

    this.storage.get('webrtc').then((webrtc) => {
      this.webrtc=webrtc;
    })


  }


  ngAfterViewInit(){
  this.reset();
  }
 


  ////////
  InitializeApiRTC() {
    console.log("initalisation webrtc 22");
    apiRTC.init({
      apiKey: "ccbdd2390d18fd5be78ed81ecd040b5e",
      token : "HJ3jw.oVNyTbwfMk}C9?qsG2X2PIR:JlIgn+dHmh-PDeejAL",
      // apiCCId : "2",
      onReady: (e) => {
        this.sessionReadyHandler(e);
      }
    });
    this.storage.get('iduser').then((val) => {
      this.fkuser=val;
    this.myCallId = apiRTC.session.apiCCId;
    var data = JSON.stringify({ 
      userid:  this.fkuser,
      webrtc: this.myCallId,
    });

    console.log(data);
    this.redditService.UpdateWebrtc(data)  
    .toPromise()
    .then((response) =>
   {
  console.log(response);
    if(response.status == 'success') {
      console.log("Mise à jour del'idwebrtc");
    }})})
  }


  /////////////////////

  sessionReadyHandler(e) {
    this.myCallId = apiRTC.session.apiCCId;
    this.InitializeControls();
    this.AddEventListeners();
    this.InitializeWebRTCClient();
    
  }

  InitializeControls() {
    this.showCall = false;
    this.showAnswer = false;
    this.showHangup = false;
    this.showReject = false;
    this.showRemoteVideo=false;
  }

  InitializeWebRTCClient() {
    this.webRTCClient = apiRTC.session.createWebRTCClient({
      status: "status" //Optionnal
    });
      this.webRTCClient.setAllowMultipleCalls(false);
      // this.webRTCClient.setVideoBandwidth(300);
      // this.webRTCClient.setUserAcceptOnIncomingCall(false);
       this.webRTCClient.setUserAcceptOnIncomingCallBeforeGetUserMedia(true);
  }

 
  AddEventListeners() {

    apiRTC.addEventListener("userMediaSuccess", (e) => {
      this.showStatus = true;
      this.showMyVideo = false;
      this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId, {
        width: "128px",
        height: "96px"
      }, true);
    });
  
    apiRTC.addEventListener("incomingCall", (e) => {
      console.log("incomingCall");


     this.InitializeControlsForIncomingCall();
     this.incomingCallId = e.detail.callId;
     this.sound=true;
     this.play();
    });

    apiRTC.addEventListener("hangup", (e) => {

      console.log("IMAGE");
      if (e.detail.lastEstablishedCall === true) {
     //   this.InitializeControlsForHangup();
      }
      this.status = this.status + "<br/> The call has been hunged up due to the following reasons <br/> " + e.detail.reason;
    //  this.RemoveMediaElements(e.detail.callId);
    });

    apiRTC.addEventListener("remoteStreamAdded", (e) => {
      this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "remote", 'remoteElt-' + e.detail.callId, {
        width: "400px",
      height: "700px"
        //height: "225px"
      }, false);
    });
  }

  InitializeControlsForIncomingCall() {
    this.showCall = false;
    this.showAnswer = true;
    this.showHistory= false;
    console.log(  this.showAnswer);
  }

  InitializeControlsForHangup() {
    this.showCall = false;
    this.showAnswer = false;
    this.showReject = false;
    this.showHangup = false;
  }

  UpdateControlsOnReject() {
    this.showAnswer = false;
    this.showReject = false;
    this.showHangup = false;
    this.showCall = false;
  }

  RemoveMediaElements(callId) {
    //this.webRTCClient.removeElementFromDiv('mini', 'miniElt-' + callId);
    this.webRTCClient.removeElementFromDiv('remote', 'remoteElt-' + callId);
  }

  AddStreamInDiv(stream, callType, divId, mediaEltId, style, muted) {
    let mediaElt = null;
    let divElement = null;
  

    if (callType === 'audio') {
      mediaElt = document.createElement("audio");
    } else {
      mediaElt = document.createElement("video");
    }

    mediaElt.id = mediaEltId;
    mediaElt.autoplay = false;
   // mediaElt.muted = 0;
  //  mediaElt.muted = true;
    mediaElt.style.width = style.width;
    mediaElt.style.height = style.height;

    divElement = document.getElementById(divId);
    divElement.appendChild(mediaElt);

    this.webRTCClient.attachMediaStream(mediaElt, stream);
  }

  MakeCall(calleeId) {
    var callId = this.webRTCClient.call(calleeId);
    if (callId != null) {
      this.incomingCallId = callId;
      this.showHangup = true;
    }
  }

  HangUp() {
    this.sound=false;
    this.webRTCClient.hangUp(this.incomingCallId);
    this.showAnswer = false;
    this.showHistory=true;

  }

  AnswerCall(incomingCallId) {
    this.sound=false;
    this.stop();
    this.showRemoteVideo=true;
    this.showAnswer = false;
    this.showCommand= true;
    this.webRTCClient.acceptCall(incomingCallId);
   // this.UpdateControlsOnAnswer();
  }

  RejectCall(incomingCallId) {
    this.sound=false;
    this.stop();
    this.webRTCClient.refuseCall(incomingCallId);
    this.UpdateControlsOnReject();
    this.RemoveMediaElements(incomingCallId);
    this.stop();
  }
  



  ///////////////FONCTION NOTIFICATION////



doRefresh(event) {
setTimeout(() => {
  this.page=1 ;
  this.storage.get('iduser').then((val) => {
    this.fkuser=val;

  console.log(this.fkuser);

  this.redditService.notif(this.page,this.fkuser).subscribe(data=>{
    console.log(data);
    this.posts=data.listing;
    this.items=data.items;
    this.pages=data.page;
    this.currentpage=data.currentpage;
    });
  })
  event.target.complete();
}, 2000);
}


doInfinite(event) {
if  (this.page<this.pages){
     this.page = this.page +1 ;
    setTimeout(() => {
   
      this.redditService.notif(this.page,this.fkuser).subscribe(data=>{
        let posts=data.listing;
        for(let post of posts){
          this.posts.push(post); 
        }
        this.items=data.items;
        this.pages=data.page;
      });
      event.target.complete();
    }, 1000);
    } else{
      setTimeout(() => {
        event.target.complete();
        this.reset();
    }, 1000);
    
    }
  }


  getnotif(){
    this.storage.get('iduser').then((iduser) => {
      this.fkuser=iduser;  
      this.page=1  
    this.redditService.notif(this.page,this.fkuser).subscribe(data=>{
      console.log(data);
      this.posts=data.listing;
      this.items=data.items;
      this.pages=data.page;
      this.currentpage=data.currentpage;
      });
    })
  }



  async reset(){
    console.log("REST");
    this.storage.get('iduser').then((iduser) => {
      console.log("Iduser");
      this.iduserapi=iduser;    
    
  
    console.log("ID USER");
    console.log(this.iduserapi);
    console.log("SESSESION READER");
  
    console.log("start session");
    console.log("mon call id");
    this.myCallId = apiRTC.session.apiCCId;
    var data = JSON.stringify({ 
      userid: this.iduserapi,
      webrtc: this.myCallId,
    });
  
    console.log(data);
    this.redditService.UpdateWebrtc(data)  
              .toPromise()
              .then((response) =>
              {
                console.log(response);
              if(response.status == 'success') {
           
  
         
                
                }
              })
  
            })
    }
  


  ///////////////FONCTION 

 infointerphone(){

  this.storage.get('iduser').then((iduser) => {
    this.fkuser=iduser;    
  this.redditService.listingbyiduser(this.fkuser).subscribe(data=>{
    this.nameblue=data.listing[0].nameblue;
    this.idinterphone=data.listing[0].id;
    });
  })
}


  async open() {

  const loader = await this.loadingController.create({
    duration: 2000
  });

  this.HangUp();
  this.showAnswer=false; 
  this.showCommand=false;
  this.showRemoteVideo=false;

  this.RemoveMediaElements(this.incomingCallId);
  this.redditService.notifbyid(this.idinterphone).subscribe(data=>{
        this.idnotif=data.listing[0].idnotif;
        console.log(this.idnotif);
        var data2 = JSON.stringify({ 
          id:this.idnotif,
          title:"Entrée autorisée", 
          notif:1,
          });
    this.redditService.updateNotif(data2)  
    .toPromise()
    .then((response) =>
    {if(response[0].status="success"){
        loader.present();
        loader.onWillDismiss().then(async l => {
          const toast = await this.toastCtrl.create({
            showCloseButton: true,
            cssClass: 'bg-profile',
            message: 'Autorisation ouverture porte',
            duration: 3000,
            position: 'bottom',
            closeButtonText: 'Fermer'
          });
    
          toast.present();
          this.getnotif();
          this.showHistory=true;
        });
      }
    })
    });
}

async close() {
  const loader = await this.loadingController.create({
    duration: 2000
  });
  this.HangUp();
  this.showAnswer=false; 
  this.showCommand=false;
  this.RemoveMediaElements(this.incomingCallId);

  this.redditService.notifbyid(this.idinterphone).subscribe(data=>{
    this.idnotif=data.listing[0].idnotif;
    console.log(this.idnotif);
    var data2 = JSON.stringify({ 
      id:this.idnotif,
      title:"Entrée refusée", 
      notif:2,
      });
this.redditService.updateNotif(data2)  
.toPromise()
.then((response) =>
{if(response[0].status="success"){
    loader.present();
    loader.onWillDismiss().then(async l => {
      const toast = await this.toastCtrl.create({
        showCloseButton: true,
        cssClass: 'bg-profile',
        message: 'Entrée non autorisée',
        duration: 3000,
        position: 'bottom',
        closeButtonText: 'Fermer'
      });

      toast.present();
      this.getnotif();
      this.showHistory=true;
    });
  }
})
});
  }



  //////////SONNERIE



   play(){
    this.ringtones.playRingtone('content://media/internal/audio/media/118');
    if(this.sound){
    setTimeout(() => {
      this.play();
    }, 1000);
  }
  }


  stop(){
    this.sound=false;
    this.ringtones.stopRingtone('content://media/internal/audio/media/118');
   }

async logout(event, item) {
  const alert = await this.alertController.create({
    header: 'Deconnexion',
    message: 'Voulez-vous vraiment vous déconnecter ? ',
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
        }
      }, {
        text: 'Oui',
        handler: () => { 
          setTimeout(() => { 

            this.authService.logout()
            this.router.navigateByUrl('/login');
         }, 1000); 
        }}]
    });
  await alert.present();
 }



 checkPermissions(){
  this.androidPermissions.requestPermissions(
    [this.androidPermissions.PERMISSION.CAMERA,
      this.androidPermissions.PERMISSION.RECORD_AUDIO
    ]
  );

  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
    success => console.log("Hey you have permission"),
    err => {
      console.log("Uh oh, looks like you don't have permission");
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA);
    }
  );

  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(
    success => console.log("Hey you have permission"),
    err => {
      console.log("Uh oh, looks like you don't have permission");
      this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO);
    }
  );
}


}

