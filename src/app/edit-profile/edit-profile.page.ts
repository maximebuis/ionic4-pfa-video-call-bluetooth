import { Component, OnInit } from '@angular/core';
import { NavController, LoadingController, PopoverController, AlertController, ToastController } from '@ionic/angular';
import { RedditService } from 'src/providers/reddit-service';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  iduser: any;
  id: number;
  table: string="customers_auth";
  category:string="page";
  view:boolean;
  push: boolean=false;
  data: any;
  posts: any;
  content: string="";
  image:string="";
  title: string="";
  viewdata: any;
  lastname: any;
  firstname: any;
  email: any;
  address: any;
  city: any;
  cp: any;
  phone: any;
  label: any;
  selectedImage: string;
  base64Image: string;
  photos: any;
  
    constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,public alertController: AlertController,private route: ActivatedRoute,
      public loadingController:LoadingController, public redditService:RedditService, private router: Router,  public toastCtrl: ToastController,
      private storage: Storage, private camera: Camera, private alertCtrl: AlertController) {
  
     }
  ngOnInit() {


    this.storage.get('iduser').then((val) => {
      this.id=val;
    this.redditService.postByid(this.table, this.id).subscribe(data=>{
      this.posts=data.items;
      this.category= data.items[0].category;
      this.firstname=data.items[0].firstname;
      this.lastname=data.items[0].lastname;
      this.city=data.items[0].city;
      this.address=data.items[0].address;
      this.cp=data.items[0].cp;
      this.email=data.items[0].email;
      this.phone=data.items[0].phone;
      this.image=data.items[0].image;
      this.selectedImage = "data:image/jpeg;base64,"+this.image;
    })
  });
  }

  async  sendData() {
    const loader = await this.loadingController.create({
      duration: 2000
    });
    const alert = await this.alertController.create({
      header: 'Enregistrer',
      message: 'Voulez-vous vraiment enregister ? ',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Oui',
          handler: () => { 
            var data = JSON.stringify({ 
              id:this.id,
              lastname: this.lastname,
              firstname: this.firstname,
              email: this.email,
              address: this.address,
              city: this.city,
              cp: this.cp,
              phone:this.phone,
              image:this.image,
              });
        this.redditService.updateUser(data)  
        .toPromise()
        .then((response) =>
        {if(response[0].status="success"){
            loader.present();
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                showCloseButton: true,
                cssClass: 'bg-profile',
                message: 'Mise à jour du profil',
                duration: 3000,
                position: 'bottom',
                closeButtonText: 'Fermer'
              });
        
              toast.present();
            this.navCtrl.navigateForward('/tabs');
            });
      
        }
        })}}]
      });
      await alert.present();
  
   }


   async accessCamera1(){

 
  const options: CameraOptions = {
      quality: 80,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      targetWidth: 600,
      targetHeight: 400,
      saveToPhotoAlbum: false
  };
  this.camera.getPicture(options).then(
      imageData => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.image=imageData;

       this.photos.push(this.base64Image);
   
     },
     err => {
       console.log(err);
     }
  );
  }

  
  accessGallery1(){
    const options: CameraOptions = {
        quality: 80,
        correctOrientation: true,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 600,
        targetHeight: 400,
        saveToPhotoAlbum: false
    };
    this.camera.getPicture(options).then(
        imageData => {
         this.base64Image = "data:image/jpeg;base64," + imageData;
         this.image=imageData;
         this.photos.push(this.base64Image);
        /* let alert = this.alertCtrl.create({
          title: 'Enregistrement',
          message: 'Voulez-vous enregistrer la photo de profil ?',
          buttons: [{text: 'Annuler',role: 'cancel',handler: () => {}},
            {
              text: 'Oui',
              handler: () => {
                this.doSave();
              }
            }
          ]
        });
        alert.present();*/
       },
       err => {
         console.log(err);
       }
    );
    }
}
