import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RedditService } from 'src/providers/reddit-service';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';
import { StorageService } from 'src/providers/storage';
import { AuthService } from 'src/providers/authService';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NativeRingtones } from '@ionic-native/native-ringtones/ngx';

import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonicStorageModule.forRoot()
  ],
  providers: [
    BackgroundMode,
    NativeRingtones,
    AndroidPermissions,
    NativeAudio,
    Camera,
    Storage,
    HTTP,
    RedditService,
    StorageService,
    AuthService,
    StatusBar,
    Storage,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent],
 
})


export class AppModule {}

/*
   <ion-item *ngFor="let item of posts">
        <ion-label>       <ion-icon name="albums"  slot="start"></ion-icon><b>{{item.id}}</b> </ion-label>  
     
        <ion-label text-left> 
    {{item.title}} 
    </ion-label> 
    
        <ion-label ><b>{{item.category}} </b> </ion-label>  
          <div class="item-note">
              <ion-label color="dark" position="stacked"> Date de publication </ion-label>
              <div style="padding-top:5px;">{{item.published | date: 'dd/MM/yyyy'}}</div>
            </div>
     
      </ion-item>*/

/*
<ion-card class="welcome-card">
<img src="/assets/shapes.svg" alt="" />
<ion-card-header>
  <ion-card-subtitle>    {{item.title}} </ion-card-subtitle>
  <ion-card-title>Welcome to Ionic</ion-card-title>
</ion-card-header>
<ion-card-content>
  <p>Now that your app has been created, you'll want to start building out features and components. Check out some of the resources below for next steps.</p>
</ion-card-content>
</ion-card>
*/