import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from 'src/providers/authService';
import { Router } from '@angular/router';
import { Platform, AlertController, MenuController } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public authService: AuthService, 
    private router: Router,
    public alertController: AlertController,
    private androidPermissions: AndroidPermissions,
    public nativeAudio: NativeAudio,
    private backgroundMode: BackgroundMode
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {

    
    //this.backgroundMode.enable();

    this.statusBar.styleDefault();
      
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.RECORD_AUDIO, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_NETWORK_STATE, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);



    if(this.authService.authenticated()) {
      this.router.navigateByUrl('/tabs');
    } else {
      this.router.navigateByUrl('/login');
    }
      this.splashScreen.hide();


 

    });
  }


}
