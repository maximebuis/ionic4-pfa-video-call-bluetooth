import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { RedditService } from 'src/providers/reddit-service';
import { Router } from '@angular/router';
import {Md5} from 'ts-md5/dist/md5';
import { StorageService } from 'src/providers/storage';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;
  email: any;
  password: string;
 
  token: any;
  password2: string | Int32Array;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    public redditService:RedditService,
    private router: Router,
    public alertController: AlertController,
    public storageService:StorageService,
   private storage: Storage
  ) { 

   
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    this.storage.get('email').then((email) => {
      this.email=email;
    });
    this.storage.get('password').then((password) => {
      this.password=password;
    });
  }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'email': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });
  }



  async goLogin() {
    const loader = await this.loadingCtrl.create({
      duration: 2000
    });
    this.password2=Md5.hashStr(this.password);
    var data = JSON.stringify({ 
      email:this.email,
      password: this.password2,
      }); 
    this.redditService.login(data)  
    .toPromise()
    .then(async (response) => {
    if(response.status == 'success') {

      loader.present();
      loader.onWillDismiss().then(async l => {
        const toast = await this.toastCtrl.create({
          showCloseButton: true,
          cssClass: 'bg-profile',
          message: 'Connexion réussie ',
          duration: 3000,
          position: 'bottom',
          closeButtonText: 'Fermer'
        });
  
        toast.present();
      });
      this.token=response.token;
      console.log( this.token);
      console.log( response.data);
      this.iduser=response.data.id;
      this.storage.set('iduser', this.iduser);
      this.storage.set("token",this.token);
      this.storage.set("email",this.email);
      this.storage.set("password",this.password);
      this.storage.set('webrtc', response.data.webrtc);

      setTimeout(() => { 
        handler: async () => {
          const loader = await this.loadingCtrl.create({
            duration: 2000
          });
          loader.present();
        }
        this.navCtrl.navigateRoot('/tabs');
     }, 2000); 
 
 
    } else if (response.error ==true){
      const alert = await this.alertController.create({
        header: 'Erreur',
        message: 'Identifiant ou mot de passe incorrect.',
        buttons: [
          {
            text: 'Fermer',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
            }
          }]
        });
      await alert.present();
    
    }
  
  });

  }
  iduser(arg0: string, iduser: any) {
    throw new Error("Method not implemented.");
  }


  public set(settingName,value){
    return this.storage.set(`setting:${ settingName }`,value);
  }

  public async get(settingName){
    return await this.storage.get(`setting:${ settingName }`);
  }


  async goToRegister() {
    this.router.navigateByUrl('/register');
   }
   async forgotPass() {
    this.router.navigateByUrl('/forgotpassword');
   }
   async goNfc() {
    this.router.navigateByUrl('/nfc');
   }


}
