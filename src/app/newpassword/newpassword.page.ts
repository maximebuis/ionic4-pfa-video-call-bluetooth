import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { NavController, MenuController, LoadingController, ToastController, AlertController, PopoverController } from '@ionic/angular';
import { RedditService } from 'src/providers/reddit-service';
import { Router } from '@angular/router';
import {Md5} from 'ts-md5/dist/md5';
import { Storage } from '@ionic/storage';
@Component({
  selector: 'app-newpassword',
  templateUrl: './newpassword.page.html',
  styleUrls: ['./newpassword.page.scss'],
})
export class NewpasswordPage implements OnInit {

  validations_form: FormGroup;
  matching_passwords_group: FormGroup;
  country_phone_group: FormGroup;
  formgroup: FormGroup;
  new: string="";
  password2: string | Int32Array;
  email: string = "";
  public parameter1: string;
  id: any;
  password: string;

  constructor( public popoverCtrl: PopoverController,public menu: MenuController ,
    public loadingController:LoadingController,  public redditService:RedditService,  public toastCtrl: ToastController,  public loadingCtrl: LoadingController, 
    private formBuilder: FormBuilder, private router: Router, private storage: Storage ) {
  }


  ngOnInit() {

    this.storage.get('iduser').then((val) => {
      this.id=val;
    })

    this.validations_form = this.formBuilder.group({
   
        password: new FormControl('', Validators.compose([
        Validators.maxLength(25),
        Validators.minLength(5),
        Validators.required
      ])),
   
    });
  }

  validation_messages = {
    'password': [
      { type: 'required', message: 'Mot de passe requis.' },
      { type: 'minlength', message: '5 caractères minimum' },
    ]
  };

  async save() {

    const loader = await this.loadingCtrl.create({
      duration: 2000
    });
    this.password2 = Md5.hashStr(this.password);
    var data = JSON.stringify({
      id: this.id,
      password: this.password2,
     
    });
  
    console.log(data);
  this.storage.set('password', this.password);
    this.redditService.updateuserpassword(data)
      .toPromise()
      .then((response) => {
        if(response[0].status == 'success') {
          loader.present();
          loader.onWillDismiss().then(async l => {
            const toast = await this.toastCtrl.create({
              showCloseButton: true,
              cssClass: 'bg-profile',
              message: 'Changement de mot passe. ',
              duration: 1000,
              position: 'bottom',
              closeButtonText: 'Fermer'
            });
      
            toast.present();
          });
          setTimeout(() => { 
            this.router.navigateByUrl('/tabs');
         }, 1000); 


      } else if (response[0].status == 'error'){
        
      }
      })
      .catch((error) => { console.log(error) });
  }
 


}
