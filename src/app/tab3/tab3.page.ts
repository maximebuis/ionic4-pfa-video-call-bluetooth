import { Component, OnInit } from '@angular/core';
import { NavController, PopoverController, AlertController, MenuController, LoadingController, NavParams, ToastController } from '@ionic/angular';
import { RedditService } from 'src/providers/reddit-service';
import { Router } from '@angular/router';
import { AuthService } from 'src/providers/authService';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  posts: any;
  items: any;
  pages: any;

  constructor(public navCtrl: NavController, public popoverCtrl: PopoverController,public alertController: AlertController, public menu: MenuController ,
    public loadingController:LoadingController,  public redditService:RedditService, 
    private router: Router,  public toastCtrl: ToastController,
    public authService: AuthService ) {
  }


  ngOnInit() {
    this.redditService.getPageList().subscribe(data => {
      this.posts=data.listing;
      this.items=data.items;
    })
  }
ionViewWillEnter(){
      this.redditService.getPageList().subscribe(data=>{
        this.posts=data.listing;
        this.items=data.items;
        });
}
doRefresh(event) {
  setTimeout(() => {
    this.redditService.getPageList().subscribe(data=>{
      this.posts=data.listing;
      this.items=data.items;
      });
    event.target.complete();
  }, 2000);
}

    async logout(event, item) {
    const alert = await this.alertController.create({
      header: 'Deconnexion',
      message: 'Voulez-vous vraiment vous déconnecter ? ',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Oui',
          handler: () => { 
            setTimeout(() => { 

              this.authService.logout()
              this.router.navigateByUrl('/login');
           }, 1000); 
          }}]
      });
    await alert.present();
   }

   async view(event, item) {
    this.router.navigateByUrl('/page/' + item.id);
   }

   async editprofil() {
    this.router.navigateByUrl('/edit-profile');
   }

   async newpassword() {
    this.router.navigateByUrl('/newpassword');
   }
}
