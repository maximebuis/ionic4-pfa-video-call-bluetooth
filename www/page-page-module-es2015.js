(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["page-page-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/page/page.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/page/page.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-header>\n    <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button\n          [text]=\"Retour\"\n          [icon]=\"buttonIcon\">\n      </ion-back-button>\n          <ion-title>{{title}}</ion-title>\n  \n      </ion-buttons>\n    </ion-toolbar>\n  </ion-header>\n<ion-content  padding >\n<ion-list no-border>\n    <div [innerHTML]=\"content\" class=\"padding\"></div>\n  </ion-list>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/page/page-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/page/page-routing.module.ts ***!
  \*********************************************/
/*! exports provided: PagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagePageRoutingModule", function() { return PagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./page.page */ "./src/app/page/page.page.ts");




const routes = [
    {
        path: '',
        component: _page_page__WEBPACK_IMPORTED_MODULE_3__["PagePage"]
    }
];
let PagePageRoutingModule = class PagePageRoutingModule {
};
PagePageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], PagePageRoutingModule);



/***/ }),

/***/ "./src/app/page/page.module.ts":
/*!*************************************!*\
  !*** ./src/app/page/page.module.ts ***!
  \*************************************/
/*! exports provided: PagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagePageModule", function() { return PagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./page-routing.module */ "./src/app/page/page-routing.module.ts");
/* harmony import */ var _page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page.page */ "./src/app/page/page.page.ts");







let PagePageModule = class PagePageModule {
};
PagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _page_routing_module__WEBPACK_IMPORTED_MODULE_5__["PagePageRoutingModule"]
        ],
        declarations: [_page_page__WEBPACK_IMPORTED_MODULE_6__["PagePage"]]
    })
], PagePageModule);



/***/ }),

/***/ "./src/app/page/page.page.scss":
/*!*************************************!*\
  !*** ./src/app/page/page.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2UvcGFnZS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/page/page.page.ts":
/*!***********************************!*\
  !*** ./src/app/page/page.page.ts ***!
  \***********************************/
/*! exports provided: PagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagePage", function() { return PagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/reddit-service */ "./src/providers/reddit-service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





let PagePage = class PagePage {
    constructor(navCtrl, popoverCtrl, alertController, route, loadingController, redditService, router, toastCtrl) {
        this.navCtrl = navCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertController = alertController;
        this.route = route;
        this.loadingController = loadingController;
        this.redditService = redditService;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.table = "page";
        this.category = "page";
        this.push = false;
        this.content = "";
        this.image = "";
        this.title = "";
    }
    ngOnInit() {
        this.route.params.subscribe(params => {
            this.id = params['id'];
        });
        this.redditService.postByid(this.table, this.id).subscribe(data => {
            this.posts = data.items;
            this.title = data.items[0].title;
            this.content = data.items[0].content;
            this.viewdata = data.items[0].view;
        });
    }
};
PagePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_3__["RedditService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
PagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-page',
        template: __webpack_require__(/*! raw-loader!./page.page.html */ "./node_modules/raw-loader/index.js!./src/app/page/page.page.html"),
        styles: [__webpack_require__(/*! ./page.page.scss */ "./src/app/page/page.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_3__["RedditService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], PagePage);



/***/ })

}]);
//# sourceMappingURL=page-page-module-es2015.js.map