(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/tab1/tab1.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/tab1/tab1.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar  color=\"primary\">\n      <ion-title>\n      PFA interphone\n      </ion-title>\n      <ion-buttons slot=\"end\"(click)=\"reset()\">\n        <ion-button>\n          <ion-icon name=\"refresh\"></ion-icon>\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-header>\n    \n<ion-content>\n    <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n      <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n\n      <ion-button *ngIf=\"myCallId\" expand=\"full\" color=\"light\" > Connexion : en marche {{myCallId}}</ion-button>\n      <ion-button *ngIf=\"myCallId==null\" expand=\"full\" color=\"light\" (click)=\"reset()\"> Mettre à jour ma connexion </ion-button>\n\n      <div *ngIf=\"showHistory\" >\n    <ion-list>\n            <ion-item  *ngFor=\"let item of posts\" >\n              <ion-thumbnail slot=\"start\">\n                <div *ngIf=\"item.notif==1\">\n                <img src=\"../assets/imgs/enter.png\" width=\"100px\">\n               </div>\n               <div *ngIf=\"item.notif==0\">\n                <img src=\"../assets/imgs/phone.png\" width=\"100px\">\n              </div>\n                <div *ngIf=\"item.notif==2\">\n                <img src=\"../assets/imgs/close.png\" width=\"100px\">\n              </div>\n              </ion-thumbnail>\n              <ion-label class=\"ion-text-wrap\">\n                {{item.title}} le <b>{{item.days}}</b> à <b>{{item.hour}}h{{item.minute}}</b>\n              </ion-label>\n            </ion-item>\n    </ion-list>\n  </div>\n  \n    <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n        <ion-infinite-scroll-content\n          loadingSpinner=\"bubbles\"\n          loadingText=\"Chargement…\">\n        </ion-infinite-scroll-content>\n      </ion-infinite-scroll>\n  \n\n      <!-- appl *ngIf=\"showAnswer\"-->\n      <div *ngIf=\"showAnswer\" style=\"padding-top:50px;\">\n      <ion-row style=\"text-align: center; width: 200px;margin: auto; display: block;\">\n        <h1> Appel video en cours </h1>\n      </ion-row>\n      <ion-row>\n        <ion-col style=\"text-align: center; width: 50px;margin: auto; display: block;\">\n         <img src=\"../assets/imgs/accept.png\" width=\"100px\" (click)='AnswerCall(incomingCallId)'>\n         <div style=\"text-align: center; width: 100px;margin: auto; display: block;\">\n           Accepter\n         </div>\n         </ion-col>\n         <ion-col style=\"text-align: center; width: 100px;margin: auto; display: block;\">\n         <img src=\"../assets/imgs/refuse.jpg\" width=\"100px\"  (click)='HangUp()'>\n         <div style=\"text-align: center; width: 100px;margin: auto; display: block;\">\n        Refuser\n         </div>\n       </ion-col>\n       </ion-row>\n      </div>  \n       \n  <div  *ngIf=\"showCommand\"  style=\"padding-top:350px;\"  text-center>\n             \n        \n\n\n        <ion-item>\n          <ion-thumbnail slot=\"start\">\n            <img src=\"../assets/imgs/enter.png\" width=\"100px\">\n          </ion-thumbnail>\n          <ion-label>{{nameblue}}</ion-label>\n  \n      \n        </ion-item>\n       <ion-item>\n       \n\n        <ion-button size=\"meduim\" color=\"light\" (click)='close()'> Refuser </ion-button>\n        <ion-button size=\"meduim\" (click)='open()' >\n          <ion-icon name=\"open\"></ion-icon>Ouvrir</ion-button>\n      </ion-item>\n      </div>  \n\n\n  <div id=\"remote\" style=\"position:absolute; top:2px;right:0px;background-color: darkgrey; left: 0; z-index: 0;\"></div>\n\n            \n</ion-content>\n\n\n"

/***/ }),

/***/ "./src/app/tab1/tab1.module.ts":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tab1.page */ "./src/app/tab1/tab1.page.ts");







let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild([{ path: '', component: _tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"] }])
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_6__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ }),

/***/ "./src/app/tab1/tab1.page.scss":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host .welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n:host .circular {\n  width: 60px;\n  height: 60px;\n  position: inherit;\n  border-radius: 50%;\n  -webkit-border-radius: 150px;\n  -moz-border-radius: 150px;\n  background-size: 100%;\n  margin-top: 20px;\n  margin-bottom: 10px;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .call {\n  border-radius: 15px 50px;\n  background: #0ec254;\n  border-radius: 50%;\n  padding: 100px;\n  width: 50px;\n  height: 50px;\n}\n:host .call2 {\n  border-radius: 15px 50px;\n  background: red;\n  border-radius: 50%;\n  padding: 100px;\n  width: 50px;\n  height: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9jYXByaS9EZXNrdG9wL3BmYTcvc3JjL2FwcC90YWIxL3RhYjEucGFnZS5zY3NzIiwic3JjL2FwcC90YWIxL3RhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtBQ0hGO0FES0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0QkFBQTtFQUNBLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ0hGO0FET0E7RUFDRSx3QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNMRjtBRFFBO0VBQ0Usd0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNORiIsImZpbGUiOiJzcmMvYXBwL3RhYjEvdGFiMS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuXG46aG9zdCB7XG4ud2VsY29tZS1jYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG4uY2lyY3VsYXIge1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xuICBwb3NpdGlvbjogaW5oZXJpdDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDE1MHB4O1xuICAtbW96LWJvcmRlci1yYWRpdXM6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIC8vIG1hcmdpbi1ib3R0b206IDUwcHg7XG59XG5cbi5jYWxsIHtcbiAgYm9yZGVyLXJhZGl1czogMTVweCA1MHB4O1xuICBiYWNrZ3JvdW5kOiAjMGVjMjU0O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBhZGRpbmc6IDEwMHB4O1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uY2FsbDIge1xuICBib3JkZXItcmFkaXVzOiAxNXB4IDUwcHg7XG4gIGJhY2tncm91bmQ6IHJlZDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiAxMDBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbn1cbn0iLCI6aG9zdCAud2VsY29tZS1jYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG46aG9zdCAuY2lyY3VsYXIge1xuICB3aWR0aDogNjBweDtcbiAgaGVpZ2h0OiA2MHB4O1xuICBwb3NpdGlvbjogaW5oZXJpdDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDE1MHB4O1xuICAtbW96LWJvcmRlci1yYWRpdXM6IDE1MHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG46aG9zdCAuY2FsbCB7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHggNTBweDtcbiAgYmFja2dyb3VuZDogIzBlYzI1NDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBwYWRkaW5nOiAxMDBweDtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbn1cbjpob3N0IC5jYWxsMiB7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHggNTBweDtcbiAgYmFja2dyb3VuZDogcmVkO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBhZGRpbmc6IDEwMHB4O1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/tab1/tab1.page.ts":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/providers/reddit-service */ "./src/providers/reddit-service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/native-audio/ngx */ "./node_modules/@ionic-native/native-audio/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_providers_authService__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/providers/authService */ "./src/providers/authService.ts");
/* harmony import */ var _ionic_native_native_ringtones_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/native-ringtones/ngx */ "./node_modules/@ionic-native/native-ringtones/ngx/index.js");
/* harmony import */ var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/android-permissions/ngx */ "./node_modules/@ionic-native/android-permissions/ngx/index.js");










let Tab1Page = class Tab1Page {
    constructor(navCtrl, storage, popoverCtrl, redditService, alertController, router, toastCtrl, loadingController, loadingCtrl, nativeAudio, authService, platform, ringtones, androidPermissions) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.popoverCtrl = popoverCtrl;
        this.redditService = redditService;
        this.alertController = alertController;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.loadingController = loadingController;
        this.loadingCtrl = loadingCtrl;
        this.nativeAudio = nativeAudio;
        this.authService = authService;
        this.platform = platform;
        this.ringtones = ringtones;
        this.androidPermissions = androidPermissions;
        this.table = "posts";
        this.word1 = "title";
        this.word2 = "content";
        this.category = 0;
        this.word = "";
        this.wordid = "";
        this.showRemoteVideo = false;
        this.showMyVideo = false;
        this.showCommand = false;
        this.showHistory = true;
        this.incomingCallId = 0;
        this.checkPermissions();
        this.InitializeApiRTC();
        this.ringtones.getRingtone().then((ringtones) => { console.log(ringtones); });
    }
    ionViewWillEnter() {
        this.getnotif();
        this.infointerphone();
        this.reset();
    }
    ngOnInit() {
        this.page = 1;
        console.log(this.fkuser);
        this.storage.get('customer_id').then((iduser) => {
            this.iduserapi = iduser;
        });
        this.storage.get('iduser').then((iduser) => {
            this.iduserapi = iduser;
        });
        this.storage.get('webrtc').then((webrtc) => {
            this.webrtc = webrtc;
        });
    }
    ngAfterViewInit() {
        this.reset();
    }
    ////////
    InitializeApiRTC() {
        console.log("initalisation webrtc 22");
        apiRTC.init({
            apiKey: "ccbdd2390d18fd5be78ed81ecd040b5e",
            token: "HJ3jw.oVNyTbwfMk}C9?qsG2X2PIR:JlIgn+dHmh-PDeejAL",
            // apiCCId : "2",
            onReady: (e) => {
                this.sessionReadyHandler(e);
            }
        });
        this.storage.get('iduser').then((val) => {
            this.fkuser = val;
            this.myCallId = apiRTC.session.apiCCId;
            var data = JSON.stringify({
                userid: this.fkuser,
                webrtc: this.myCallId,
            });
            console.log(data);
            this.redditService.UpdateWebrtc(data)
                .toPromise()
                .then((response) => {
                console.log(response);
                if (response.status == 'success') {
                    console.log("Mise à jour del'idwebrtc");
                }
            });
        });
    }
    /////////////////////
    sessionReadyHandler(e) {
        this.myCallId = apiRTC.session.apiCCId;
        this.InitializeControls();
        this.AddEventListeners();
        this.InitializeWebRTCClient();
    }
    InitializeControls() {
        this.showCall = false;
        this.showAnswer = false;
        this.showHangup = false;
        this.showReject = false;
        this.showRemoteVideo = false;
    }
    InitializeWebRTCClient() {
        this.webRTCClient = apiRTC.session.createWebRTCClient({
            status: "status" //Optionnal
        });
        this.webRTCClient.setAllowMultipleCalls(false);
        // this.webRTCClient.setVideoBandwidth(300);
        // this.webRTCClient.setUserAcceptOnIncomingCall(false);
        this.webRTCClient.setUserAcceptOnIncomingCallBeforeGetUserMedia(true);
    }
    AddEventListeners() {
        apiRTC.addEventListener("userMediaSuccess", (e) => {
            this.showStatus = true;
            this.showMyVideo = false;
            this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "mini", 'miniElt-' + e.detail.callId, {
                width: "128px",
                height: "96px"
            }, true);
        });
        apiRTC.addEventListener("incomingCall", (e) => {
            console.log("incomingCall");
            this.InitializeControlsForIncomingCall();
            this.incomingCallId = e.detail.callId;
            this.sound = true;
            this.play();
        });
        apiRTC.addEventListener("hangup", (e) => {
            console.log("IMAGE");
            if (e.detail.lastEstablishedCall === true) {
                //   this.InitializeControlsForHangup();
            }
            this.status = this.status + "<br/> The call has been hunged up due to the following reasons <br/> " + e.detail.reason;
            //  this.RemoveMediaElements(e.detail.callId);
        });
        apiRTC.addEventListener("remoteStreamAdded", (e) => {
            this.webRTCClient.addStreamInDiv(e.detail.stream, e.detail.callType, "remote", 'remoteElt-' + e.detail.callId, {
                width: "400px",
                height: "700px"
                //height: "225px"
            }, false);
        });
    }
    InitializeControlsForIncomingCall() {
        this.showCall = false;
        this.showAnswer = true;
        this.showHistory = false;
        console.log(this.showAnswer);
    }
    InitializeControlsForHangup() {
        this.showCall = false;
        this.showAnswer = false;
        this.showReject = false;
        this.showHangup = false;
    }
    UpdateControlsOnReject() {
        this.showAnswer = false;
        this.showReject = false;
        this.showHangup = false;
        this.showCall = false;
    }
    RemoveMediaElements(callId) {
        //this.webRTCClient.removeElementFromDiv('mini', 'miniElt-' + callId);
        this.webRTCClient.removeElementFromDiv('remote', 'remoteElt-' + callId);
    }
    AddStreamInDiv(stream, callType, divId, mediaEltId, style, muted) {
        let mediaElt = null;
        let divElement = null;
        if (callType === 'audio') {
            mediaElt = document.createElement("audio");
        }
        else {
            mediaElt = document.createElement("video");
        }
        mediaElt.id = mediaEltId;
        mediaElt.autoplay = false;
        // mediaElt.muted = 0;
        //  mediaElt.muted = true;
        mediaElt.style.width = style.width;
        mediaElt.style.height = style.height;
        divElement = document.getElementById(divId);
        divElement.appendChild(mediaElt);
        this.webRTCClient.attachMediaStream(mediaElt, stream);
    }
    MakeCall(calleeId) {
        var callId = this.webRTCClient.call(calleeId);
        if (callId != null) {
            this.incomingCallId = callId;
            this.showHangup = true;
        }
    }
    HangUp() {
        this.sound = false;
        this.webRTCClient.hangUp(this.incomingCallId);
        this.showAnswer = false;
        this.showHistory = true;
    }
    AnswerCall(incomingCallId) {
        this.sound = false;
        this.stop();
        this.showRemoteVideo = true;
        this.showAnswer = false;
        this.showCommand = true;
        this.webRTCClient.acceptCall(incomingCallId);
        // this.UpdateControlsOnAnswer();
    }
    RejectCall(incomingCallId) {
        this.sound = false;
        this.stop();
        this.webRTCClient.refuseCall(incomingCallId);
        this.UpdateControlsOnReject();
        this.RemoveMediaElements(incomingCallId);
        this.stop();
    }
    ///////////////FONCTION NOTIFICATION////
    doRefresh(event) {
        setTimeout(() => {
            this.page = 1;
            this.storage.get('iduser').then((val) => {
                this.fkuser = val;
                console.log(this.fkuser);
                this.redditService.notif(this.page, this.fkuser).subscribe(data => {
                    console.log(data);
                    this.posts = data.listing;
                    this.items = data.items;
                    this.pages = data.page;
                    this.currentpage = data.currentpage;
                });
            });
            event.target.complete();
        }, 2000);
    }
    doInfinite(event) {
        if (this.page < this.pages) {
            this.page = this.page + 1;
            setTimeout(() => {
                this.redditService.notif(this.page, this.fkuser).subscribe(data => {
                    let posts = data.listing;
                    for (let post of posts) {
                        this.posts.push(post);
                    }
                    this.items = data.items;
                    this.pages = data.page;
                });
                event.target.complete();
            }, 1000);
        }
        else {
            setTimeout(() => {
                event.target.complete();
                this.reset();
            }, 1000);
        }
    }
    getnotif() {
        this.storage.get('iduser').then((iduser) => {
            this.fkuser = iduser;
            this.page = 1;
            this.redditService.notif(this.page, this.fkuser).subscribe(data => {
                console.log(data);
                this.posts = data.listing;
                this.items = data.items;
                this.pages = data.page;
                this.currentpage = data.currentpage;
            });
        });
    }
    reset() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("REST");
            this.storage.get('iduser').then((iduser) => {
                console.log("Iduser");
                this.iduserapi = iduser;
                console.log("ID USER");
                console.log(this.iduserapi);
                console.log("SESSESION READER");
                console.log("start session");
                console.log("mon call id");
                this.myCallId = apiRTC.session.apiCCId;
                var data = JSON.stringify({
                    userid: this.iduserapi,
                    webrtc: this.myCallId,
                });
                console.log(data);
                this.redditService.UpdateWebrtc(data)
                    .toPromise()
                    .then((response) => {
                    console.log(response);
                    if (response.status == 'success') {
                    }
                });
            });
        });
    }
    ///////////////FONCTION 
    infointerphone() {
        this.storage.get('iduser').then((iduser) => {
            this.fkuser = iduser;
            this.redditService.listingbyiduser(this.fkuser).subscribe(data => {
                this.nameblue = data.listing[0].nameblue;
                this.idinterphone = data.listing[0].id;
            });
        });
    }
    open() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loader = yield this.loadingController.create({
                duration: 2000
            });
            this.HangUp();
            this.showAnswer = false;
            this.showCommand = false;
            this.showRemoteVideo = false;
            this.RemoveMediaElements(this.incomingCallId);
            this.redditService.notifbyid(this.idinterphone).subscribe(data => {
                this.idnotif = data.listing[0].idnotif;
                console.log(this.idnotif);
                var data2 = JSON.stringify({
                    id: this.idnotif,
                    title: "Entrée autorisée",
                    notif: 1,
                });
                this.redditService.updateNotif(data2)
                    .toPromise()
                    .then((response) => {
                    if (response[0].status = "success") {
                        loader.present();
                        loader.onWillDismiss().then((l) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            const toast = yield this.toastCtrl.create({
                                showCloseButton: true,
                                cssClass: 'bg-profile',
                                message: 'Autorisation ouverture porte',
                                duration: 3000,
                                position: 'bottom',
                                closeButtonText: 'Fermer'
                            });
                            toast.present();
                            this.getnotif();
                            this.showHistory = true;
                        }));
                    }
                });
            });
        });
    }
    close() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loader = yield this.loadingController.create({
                duration: 2000
            });
            this.HangUp();
            this.showAnswer = false;
            this.showCommand = false;
            this.RemoveMediaElements(this.incomingCallId);
            this.redditService.notifbyid(this.idinterphone).subscribe(data => {
                this.idnotif = data.listing[0].idnotif;
                console.log(this.idnotif);
                var data2 = JSON.stringify({
                    id: this.idnotif,
                    title: "Entrée refusée",
                    notif: 2,
                });
                this.redditService.updateNotif(data2)
                    .toPromise()
                    .then((response) => {
                    if (response[0].status = "success") {
                        loader.present();
                        loader.onWillDismiss().then((l) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            const toast = yield this.toastCtrl.create({
                                showCloseButton: true,
                                cssClass: 'bg-profile',
                                message: 'Entrée non autorisée',
                                duration: 3000,
                                position: 'bottom',
                                closeButtonText: 'Fermer'
                            });
                            toast.present();
                            this.getnotif();
                            this.showHistory = true;
                        }));
                    }
                });
            });
        });
    }
    //////////SONNERIE
    play() {
        this.ringtones.playRingtone('content://media/internal/audio/media/118');
        if (this.sound) {
            setTimeout(() => {
                this.play();
            }, 1000);
        }
    }
    stop() {
        this.sound = false;
        this.ringtones.stopRingtone('content://media/internal/audio/media/118');
    }
    logout(event, item) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Deconnexion',
                message: 'Voulez-vous vraiment vous déconnecter ? ',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                        }
                    }, {
                        text: 'Oui',
                        handler: () => {
                            setTimeout(() => {
                                this.authService.logout();
                                this.router.navigateByUrl('/login');
                            }, 1000);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    checkPermissions() {
        this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA,
            this.androidPermissions.PERMISSION.RECORD_AUDIO
        ]);
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(success => console.log("Hey you have permission"), err => {
            console.log("Uh oh, looks like you don't have permission");
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA);
        });
        this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO).then(success => console.log("Hey you have permission"), err => {
            console.log("Uh oh, looks like you don't have permission");
            this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.RECORD_AUDIO);
        });
    }
};
Tab1Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_3__["RedditService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeAudio"] },
    { type: src_providers_authService__WEBPACK_IMPORTED_MODULE_7__["AuthService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _ionic_native_native_ringtones_ngx__WEBPACK_IMPORTED_MODULE_8__["NativeRingtones"] },
    { type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"] }
];
Tab1Page = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tab1',
        template: __webpack_require__(/*! raw-loader!./tab1.page.html */ "./node_modules/raw-loader/index.js!./src/app/tab1/tab1.page.html"),
        styles: [__webpack_require__(/*! ./tab1.page.scss */ "./src/app/tab1/tab1.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"], src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_3__["RedditService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"], _ionic_native_native_audio_ngx__WEBPACK_IMPORTED_MODULE_5__["NativeAudio"],
        src_providers_authService__WEBPACK_IMPORTED_MODULE_7__["AuthService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"], _ionic_native_native_ringtones_ngx__WEBPACK_IMPORTED_MODULE_8__["NativeRingtones"], _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"]])
], Tab1Page);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map