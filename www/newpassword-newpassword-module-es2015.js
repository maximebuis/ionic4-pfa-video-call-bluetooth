(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["newpassword-newpassword-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/newpassword/newpassword.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/newpassword/newpassword.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n          <ion-back-button\n          [text]=\"Retour\"\n          [icon]=\"buttonIcon\">\n      </ion-back-button>\n     \n      <ion-title>Nouveau mot de passe</ion-title>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content  padding class=\"form-content\">\n\n      <ion-list no-border>   \n      <form class=\"form\" [formGroup]=\"validations_form\"  (ngSubmit)=\"onSubmit(validations_form.value)\">    \n\n           <p>Ajoutez un  nouveau mot de passe </p>\n\n              <ion-item class=\"border\">\n                  <ion-label  position=\"floating\"  > \n                      <ion-icon name=\"lock\" item-start></ion-icon>\n                       Mot de passe *\n                  </ion-label>\n                <ion-input type=\"password\" [(ngModel)]=\"password\" formControlName=\"password\"></ion-input>\n              </ion-item>\n              <div class=\"validation-errors\">\n                <ng-container *ngFor=\"let validation of validation_messages.password\">\n                  <div class=\"error-message\" *ngIf=\"validations_form.get('password').hasError(validation.type) && (validations_form.get('password').dirty || validations_form.get('password').touched)\">\n                    {{ validation.message }}\n                  </div>\n                </ng-container>\n              </div>\n              \n                \n     \n           </form>\n           <div margin-top>\n              <ion-button icon-left size=\"medium\" expand=\"full\" shape=\"round\" color=\"dark\" (click)=\"save()\" [disabled]=\"!validations_form.valid\"\n                tappable>\n                Enregister\n              </ion-button>\n            </div>\n        \n       \n</ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/newpassword/newpassword.module.ts":
/*!***************************************************!*\
  !*** ./src/app/newpassword/newpassword.module.ts ***!
  \***************************************************/
/*! exports provided: NewpasswordPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewpasswordPageModule", function() { return NewpasswordPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _newpassword_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./newpassword.page */ "./src/app/newpassword/newpassword.page.ts");







const routes = [
    {
        path: '',
        component: _newpassword_page__WEBPACK_IMPORTED_MODULE_6__["NewpasswordPage"]
    }
];
let NewpasswordPageModule = class NewpasswordPageModule {
};
NewpasswordPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_newpassword_page__WEBPACK_IMPORTED_MODULE_6__["NewpasswordPage"]]
    })
], NewpasswordPageModule);



/***/ }),

/***/ "./src/app/newpassword/newpassword.page.scss":
/*!***************************************************!*\
  !*** ./src/app/newpassword/newpassword.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25ld3Bhc3N3b3JkL25ld3Bhc3N3b3JkLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/newpassword/newpassword.page.ts":
/*!*************************************************!*\
  !*** ./src/app/newpassword/newpassword.page.ts ***!
  \*************************************************/
/*! exports provided: NewpasswordPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewpasswordPage", function() { return NewpasswordPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/reddit-service */ "./src/providers/reddit-service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ts-md5/dist/md5 */ "./node_modules/ts-md5/dist/md5.js");
/* harmony import */ var ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");








let NewpasswordPage = class NewpasswordPage {
    constructor(popoverCtrl, menu, loadingController, redditService, toastCtrl, loadingCtrl, formBuilder, router, storage) {
        this.popoverCtrl = popoverCtrl;
        this.menu = menu;
        this.loadingController = loadingController;
        this.redditService = redditService;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.router = router;
        this.storage = storage;
        this.new = "";
        this.email = "";
        this.validation_messages = {
            'password': [
                { type: 'required', message: 'Mot de passe requis.' },
                { type: 'minlength', message: '5 caractères minimum' },
            ]
        };
    }
    ngOnInit() {
        this.storage.get('iduser').then((val) => {
            this.id = val;
        });
        this.validations_form = this.formBuilder.group({
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(25),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(5),
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required
            ])),
        });
    }
    save() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loader = yield this.loadingCtrl.create({
                duration: 2000
            });
            this.password2 = ts_md5_dist_md5__WEBPACK_IMPORTED_MODULE_6__["Md5"].hashStr(this.password);
            var data = JSON.stringify({
                id: this.id,
                password: this.password2,
            });
            console.log(data);
            this.storage.set('password', this.password);
            this.redditService.updateuserpassword(data)
                .toPromise()
                .then((response) => {
                if (response[0].status == 'success') {
                    loader.present();
                    loader.onWillDismiss().then((l) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                        const toast = yield this.toastCtrl.create({
                            showCloseButton: true,
                            cssClass: 'bg-profile',
                            message: 'Changement de mot passe. ',
                            duration: 1000,
                            position: 'bottom',
                            closeButtonText: 'Fermer'
                        });
                        toast.present();
                    }));
                    setTimeout(() => {
                        this.router.navigateByUrl('/tabs');
                    }, 1000);
                }
                else if (response[0].status == 'error') {
                }
            })
                .catch((error) => { console.log(error); });
        });
    }
};
NewpasswordPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_4__["RedditService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"] }
];
NewpasswordPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-newpassword',
        template: __webpack_require__(/*! raw-loader!./newpassword.page.html */ "./node_modules/raw-loader/index.js!./src/app/newpassword/newpassword.page.html"),
        styles: [__webpack_require__(/*! ./newpassword.page.scss */ "./src/app/newpassword/newpassword.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], src_providers_reddit_service__WEBPACK_IMPORTED_MODULE_4__["RedditService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"]])
], NewpasswordPage);



/***/ })

}]);
//# sourceMappingURL=newpassword-newpassword-module-es2015.js.map